# TabunganKu Java

## Summary
**TabunganKu** adalah project untuk memenuhi tugas mata kuliah Pemrograman Berbasis Obyek.
 Project ini merepresentasikan sistem rekening bank yang meiliki fitur sebagai berikut :
 - Cek Saldo
 - Tambah Saldo
 - Tarik Saldo
 
## Screenshot

### Tampilan Utama
 ![1](static/img/TabunganKu.png)
### Menu Cek Saldo
![2](static/img/cekSaldo.png)
### Menu Tambah Saldo
![3](static/img/tambahsaldo.png)
### Menu Tarik Saldo
![4](static/img/tarikSaldo.png)
### Menu Cek Saldo Lagi
![5](static/img/cekSaldoLagi.png)
### Finish
![6](static/img/end.png)