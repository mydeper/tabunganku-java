package id.tammam.bank;

import java.util.Scanner;
import java.io.IOException;

/**
 * Tugas PBO 1 - Membuat Class yang memiliki 3 method untuk (cek saldo, menambah saldo, mengurangi saldo)
 * Nama : Aditya Gusti Tammam
 * NPM : 14.1.03.03.0052
 */

// Deklarasi Class utama
public class Rekening {

    // Deklarasi attribute
    private String nama_bank;
    private String nama_pemilik;
    private double saldo = 0.0;

    // Method untuk menampilkan pesan sambutan
    private void greetings() {
        System.out.println("Hello " + nama_pemilik);
        System.out.println("Welcome to " + nama_bank + " !\n");
    }

    // Method untuk menampilkan menu
    private static void showMenus(Rekening rekening) {
        System.out.println("\n=============================");
        rekening.greetings();
        System.out.println("Silahkan pilih menu :");
        System.out.println("[1] Cek Saldo");
        System.out.println("[2] Tambah Saldo");
        System.out.println("[3] Tarik Saldo");
        System.out.println("[4] Keluar");
    }

    // Method untuk cek saldo
    private void checkBalance() {
        System.out.println("Sisa saldo anda sebesar " + saldo); // menampilkan saldo saat ini
    }

    // Method untuk kalkulasi penambahan saldo
    private double addBalance(double amount) {
        saldo = saldo + amount; // menambahkan saldo saat ini dengan nominal yang ditambahkan
        return saldo; // mengembalikan nilai hasil dari penambahan saldo
    }

    // Method untuk kalkulasi pengurangan saldo
    private double takeBalance(double amount) {
        if (saldo > amount) { // Cek apakah saldo mencukupi ?
            saldo = saldo - amount;
        } else {
            System.out.println("Maaf, saldo tidak mencukupi!");
        }
        return saldo;
    }

    // Method untuk melakukan pengecekan saldo
    private static void checkingBalance(Rekening rekening) {
        rekening.greetings(); // memanggil method greetings() untuk menampilkan pesan sambutan ketika cek saldo
        System.out.println("Anda memilih menu 'Cek Saldo'");
        rekening.checkBalance();
    }

    // Method untuk melakukan penambahan saldo
    private static void addingBalance(Rekening rekening) {
        // Deklarasi object input dari Class Scanner untuk menerima input dari user
        Scanner input = new Scanner(System.in);

        rekening.greetings(); // memanggil method greetings() untuk menampilkan pesan sambutan ketika tambah saldo
        System.out.println("Anda memilih menu 'Tambah Saldo'");
        System.out.print("Masukkan nominal yang ditambahkan ke saldo : ");
        double amount = input.nextDouble(); // terima input dari user berupa nominal yg ingin ditambahkan
        double saldo_awal = rekening.saldo; // simpan saldo saat ini sebelum ditambahkan untuk ditmapilkan nanti
        double saldo_akhir = rekening.addBalance(amount); // penambahan saldo dengan memanggil method addBalance

        System.out.println("\nSelamat, saldo anda berhasil ditambahkan !");
        System.out.println("Saldo awal anda : " + saldo_awal);
        System.out.println("Nominal yang ditambahkan : " + amount);
        System.out.println("Saldo akhir : " + saldo_akhir);
    }

    // Method untuk melakukan pengurangan saldo
    private static void takingBalance(Rekening rekening) {
        // Deklarasi object input dari Class Scanner untuk menerima input dari user
        Scanner input = new Scanner(System.in);

        rekening.greetings(); // memanggil method greetings() untuk menampilkan pesan sambutan ketika tambah saldo
        System.out.println("Anda memilih menu 'Tarik Saldo'");
        System.out.print("Masukkan nominal yang ditarik dari saldo : ");
        double amount = input.nextDouble(); // terima input dari user berupa nominal yg ingin ditarik
        double saldo_awal = rekening.saldo; // simpan saldo saat ini sebelum ditambahkan untuk ditmapilkan nanti
        double saldo_akhir = rekening.takeBalance(amount); // pengurangan saldo dengan memanggil method takeBalance

        if (saldo_akhir < saldo_awal) { // cek apakah saldo sudha berkurang ?
            System.out.println("\nSelamat, saldo anda berhasil ditarik !");
            System.out.println("Saldo awal anda : " + saldo_awal);
            System.out.println("Nominal yang ditarik : " + amount);
        }
        System.out.println("Saldo akhir : " + saldo_akhir);
    }

    // Deklarasi method utama
    public static void main(String[] args) throws IOException {
        Rekening bca = new Rekening(); // Deklarasi object bca dari Class Rekening
        bca.nama_bank = "Bank Central Asia"; // Update attribute nama_bank pada object bca
        bca.nama_pemilik = "Aditya Gusti Tammam"; // Update attribute nama_pemilik pada object bca

        // Deklarasi object input dari Class Scanner untuk menerima input dari user
        Scanner input = new Scanner(System.in);
        boolean is_done = false; // Set state program apakah sudah selesai ?

        while (!is_done) { // While loop : jalankan program sampai user input menu keluar
            Rekening.showMenus(bca);
            System.out.print("\nMasukkan nomor menu yang dipilih : ");
            int selected_menu = input.nextInt(); // Terima input user berupa nomor menu
            switch (selected_menu) {
                case 1:
                    Rekening.checkingBalance(bca);
                    System.out.print("\nPress Enter to continue . . .");
                    System.in.read();
                    break;
                case 2:
                    Rekening.addingBalance(bca);
                    System.out.print("\nPress Enter to continue . . .");
                    System.in.read();
                    break;
                case 3:
                    Rekening.takingBalance(bca);
                    System.out.print("\nPress Enter to continue . . .");
                    System.in.read();
                    break;
                default:
                    is_done = true; // Set is_done ke true jika user tidak memilih ketiga menu diatas untuk keluar
                    System.out.println("\nTerimakasih telah menggunakan layanan kami!");
            }
        }
    }
}
